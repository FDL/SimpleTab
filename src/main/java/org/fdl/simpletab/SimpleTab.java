package org.fdl.simpletab;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;

import me.clip.placeholderapi.PlaceholderAPI;

public class SimpleTab extends JavaPlugin {
    final String headerFormat;
    final String footerFormat;
    final int updatePeriod;

    boolean placeholderApiInstalled;
    boolean protocolLibInstalled;
    ProtocolManager protocolManager = null;

    public SimpleTab() {
        super();

        headerFormat = ChatColor.translateAlternateColorCodes('&', getConfig().getString("header"));
        footerFormat = ChatColor.translateAlternateColorCodes('&', getConfig().getString("footer"));
        updatePeriod = getConfig().getInt("updatePeriod");
    }

    @Override
    public void onEnable() {
        saveDefaultConfig();

        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            placeholderApiInstalled = true;
        } else {
            placeholderApiInstalled = false;
            getLogger().warning("Could not find PlaceholderAPI, placeholders will not work.");
        }

        if (Bukkit.getPluginManager().getPlugin("ProtocolLib") != null) {
            protocolLibInstalled = true;

            if (!Bukkit.getPluginManager().getPlugin("ProtocolLib").isEnabled()) {
                getLogger().warning(
                        "ProtocolLib is disabled! Perhaps there is an error in it? Try updating ProtocolLib.");
                getLogger().warning(
                        "If the error is still present, fill an issue on Codeberg! https://codeberg.org/FDL/SimpleTab/issues/new");
                getLogger().warning("Disabling SimpleTab...");
                this.setEnabled(false);
                return;
            }

            this.protocolManager = ProtocolLibrary.getProtocolManager();
        } else {
            protocolLibInstalled = false;
            getLogger().warning(
                    "Failed to find ProtocolLib, falling back to built-in API. Please note that the plugin will work for 1.13+ only.");
        }

        getServer().getScheduler().runTaskTimer(this, () -> {
            getServer().getOnlinePlayers().forEach((player) -> {
                String header = placeholderApiInstalled ? PlaceholderAPI.setPlaceholders(player, headerFormat)
                        : headerFormat;
                String footer = placeholderApiInstalled ? PlaceholderAPI.setPlaceholders(player, footerFormat)
                        : footerFormat;

                if (protocolLibInstalled) {
                    PacketContainer tablist = new PacketContainer(
                            PacketType.Play.Server.PLAYER_LIST_HEADER_FOOTER);
                    tablist.getChatComponents()
                            .write(0, WrappedChatComponent.fromText(header))
                            .write(1, WrappedChatComponent.fromText(footer));
                    protocolManager.sendServerPacket(player, tablist);
                } else {
                    player.setPlayerListHeaderFooter(header, footer);
                }
            });
        }, 0, updatePeriod);
    }
}
