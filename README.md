# SimpleTab

[![Modrinth Downloads](https://img.shields.io/modrinth/dt/simpletab)](https://modrinth.com/plugin/simpletab)

SimpleTab is a Paper/Spigot (+forks) plugin that implements the tablist (player list) header/footer configuration and dynamic information.

It has support for [formatting codes](https://minecraft.wiki/w/Formatting_codes) using the ampersand (&) character and support for [PlaceholderAPI](https://github.com/PlaceholderAPI/PlaceholderAPI).

## Installation

You can download the plugin on [Modrinth](https://modrinth.com/plugin/simpletab/versions) *(recommended)* or from the [Releases](https://codeberg.org/FDL/SimpleTab/releases) page.
Or you could compile the plugin from source.

## Building

Gradle is required to build the plugin.
Clone the repository, then in the folder execute `gradle build`.
The compliled .jar file is going to be in `build/libs/`

## Configuration

The configuration file will be created on first launch of your server with the plugin, it's going to be in `plugins/SimpleTab/config.yml`. It's going to look like this:

```yml
# Text above player list
header: "A cool header"

# Text below player list
footer: "And a very cool footer"

# Update period in ticks (1 tick = 50ms)
updatePeriod: 20
```

You can use `\n` for newlines as well as `&`+`char` for [formatting](https://minecraft.wiki/w/Formatting_codes). You can also use `%placeholder%` for dynamic information, if you have PlaceholderAPI installed.

Take a look at the [example configuration](https://codeberg.org/FDL/configuration/src/branch/main/plugins/SimpleTab/config.yml) that uses all 3 features.
